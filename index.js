const express = require("express");
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors({
    origin: '*',
    methods: ['GET','POST']
}));

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);

    next();
});

app.get('/', cors(), function(req, res) {
    respuesta = {
        error: true,
        codigo: 200,
        mensaje: 'Punto de inicio'
    };
    
    res.send(respuesta);
});

app.post('/suma', cors(), function (req, res) {
    if(!req.body.Num1 || !req.body.Num2) {
        respuesta = {
            error: true,
            codigo: 502,
            mensaje: 'Num1 y Num2 son requeridos'
        };
    } else {
        let Num1 = req.body.Num1;
        let Num2 = req.body.Num2;
        let res = Num1 + Num2;
        
        respuesta = {
            error: false,
            codigo: 200,
            resultado: res
        };
    }
    
    res.send(respuesta);
});

app.post('/resta', cors(), function (req, res) {
    if(!req.body.Num1 || !req.body.Num2) {
        respuesta = {
            error: true,
            codigo: 502,
            mensaje: 'Num1 y Num2 son requeridos'
        };
    } else {
        let Num1 = req.body.Num1;
        let Num2 = req.body.Num2;
        let res = Num1 - Num2;
        
        respuesta = {
            error: false,
            codigo: 200,
            resultado: res
        };
    }
    
    res.send(respuesta);
});

app.post('/multiplicacion', cors(), function (req, res) {
    if(!req.body.Num1 || !req.body.Num2) {
        respuesta = {
            error: true,
            codigo: 502,
            mensaje: 'Num1 y Num2 son requeridos'
        };
    } else {
        let Num1 = req.body.Num1;
        let Num2 = req.body.Num2;
        let res = Num1*Num2;
        
        respuesta = {
            error: false,
            codigo: 200,
            resultado: res
        };
    }
    
    res.send(respuesta);
});

app.post('/division', cors(), function (req, res) {
    if(!req.body.Num1 || !req.body.Num2) {
        respuesta = {
            error: true,
            codigo: 502,
            mensaje: 'Num1 y Num2 son requeridos'
        };
    } else {
        let Num1 = req.body.Num1;
        let Num2 = req.body.Num2;
        let res = Num1/Num2;
        
        respuesta = {
            error: false,
            codigo: 200,
            resultado: res
        };
    }
    res.send(respuesta);
});

app.listen(3000, () => {
	console.log("El servidor está inicializado en el puerto 3000");
});